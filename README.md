# Prueba / Ejemplo

Para descargar el último compilado Windows: https://gitlab.com/skryde/prueba-gitlabci/-/jobs/artifacts/master/raw/dist/prueba-windows.exe?job=build-windows

Para descargar el último compilado Linux: https://gitlab.com/skryde/prueba-gitlabci/-/jobs/artifacts/master/raw/dist/prueba-linux?job=build-linux

---

_Guia seguida: https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html_

_CI/CD YML templates: https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates_
